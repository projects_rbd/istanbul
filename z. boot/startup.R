lib   <- c("R.utils", "tidyverse", "readr", "forecast",
           "kableExtra", "gridExtra", "latex2exp", "lubridate")
noise <- lapply(lib, library, character.only = TRUE, quietly = TRUE, verbose = FALSE, warn.conflicts = FALSE)

sourceDirectory("../z. boot/Data", modifiedOnly = FALSE, onError = "error")
sourceDirectory("../z. boot/Functions", modifiedOnly = FALSE, onError = "error")
